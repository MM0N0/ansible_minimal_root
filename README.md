[![test](https://gitlab.com/MM0N0/ansible_minimal_root/badges/main/pipeline.svg)](https://gitlab.com/MM0N0/ansible_minimal_root/-/pipelines)

# Ansible Collection - minimal_root

This is a [collection](https://docs.ansible.com/ansible/devel/collections_guide/index.html)
of [Ansible](https://www.ansible.com) [roles](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_reuse_roles.html)
for setting up various systems.

It is focused on only using root, if absolutely necessary,
to make it usable on restrictive systems. 

**Note**: this is work in progress

## Collection Contents:

### Roles
- generic roles:
  - [minimal_root_setup](roles/minimal_root_setup/README.md)
  - [files](roles/files/README.md)
- application roles:
  - [apache2](roles/apache2/README.md)
  - [elasticsearch](roles/elasticsearch/README.md)
  - [postgresql](roles/postgresql/README.md)
  - [tomcat](roles/tomcat/README.md)
- use for "run once" playbooks
  - [pid](roles/pid/README.md)

## Requirements
- docker and docker-compose
- bash

note: if you are using windows, you will have to use wsl

## Installation
test setup by running:

```bash
.test/test_role.sh files
```

build docker images yourself:

```bash 
.docker/build.sh
```

## Documentation

- [docs/how_to/use_in_an_ansible_project.md](docs/how_to/use_in_an_ansible_project.md)

example project: https://gitlab.com/MM0N0/ansible_minimal_root_example


# TODOs

- update README
  - [write a role](docs/how_to/write_a_role.md)
- improve tests
  - postgresql (no tests atm)

## Contribute
...

## License

GNU General Public License v3.0 or later

See [COPYING](COPYING) to see the full text.
