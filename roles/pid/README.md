pid
=========
This role checks if a process is running and returns its pid.

It can wait until it is running or not running.

Requirements
------------
no requirements

Role Variables
--------------

| Variable          | Required | Default | Choices | Comments                                                                                                                                                                  |
|-------------------|----------|---------|---------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| pid_process_user  | no       | .+      |         | user running the application (makes detecting the pid more stable)                                                                                                        |
| pid_process_regex | yes      |         |         | regex matching the process <br/><br/>example:<br/>"/[^ ]+/java/current//bin/java ..."                                                                                     |
| pid_log           | no       | false   |         | whether to log the output or not                                                                                                                                          |
| pid_delay         | no       |         |         | delay in seconds (for until loop on the pid check)                                                                                                                        |
| pid_retries       | no       |         |         | retries (for until loop on the pid check)                                                                                                                                 |
| pid_until         | no       |         |         | condition (for until loop on the pid check)  <br/><br/>pid_out will contain the register of the shell command, which checks the pid. <br/>example:<br/>pid_out.stdout=='' |


Example Playbook
----------------
    ---
    - name: site
      gather_facts: no
      hosts: test_host
      become: yes
      become_method: sudo
      vars:
      base_dir:     "/applications"
      devops_group: "devops"
      tasks:
    
        - include_role:
          name: pid
          vars:
          pid_process_user: "tomcat_usr"
          pid_process_regex: "/[^ ]+/java/current//bin/java -Djava.util.logging.config.file=/[^ ]+/tomcat/current/conf/logging.properties -Djava.util.logging.manager=.*"
          pid_log: "true"
    
        - name: start tomcat
          changed_when: "false"
          become: "false"
          shell: "sudo systemctl start tomcat.service"
    
        - include_role:
          name: pid
          vars:
          pid_process_user: "tomcat_usr"
          pid_process_regex: "/[^ ]+/java/current//bin/java -Djava.util.logging.config.file=/[^ ]+/tomcat/current/conf/logging.properties -Djava.util.logging.manager=.*"
          pid_log: "true"
    
          pid_delay: 10
          pid_retries: 12
          pid_until: "pid_out.stdout!=''"
    
        - name: stop tomcat
          changed_when: "false"
          become: "false"
          shell: "sudo systemctl stop tomcat.service"
    
        - include_role:
          name: pid
          vars:
          pid_process_user: "tomcat_usr"
          pid_process_regex: "/[^ ]+/java/current//bin/java -Djava.util.logging.config.file=/[^ ]+/tomcat/current/conf/logging.properties -Djava.util.logging.manager=.*"
          pid_log: "true"
    
          pid_delay: 10
          pid_retries: 12
          pid_until: "pid_out.stdout==''"
