#!/usr/bin/env bash
SCRIPT_DIR=${0%/*}
source "$SCRIPT_DIR/../.dev_env/project.conf"

# build
docker build -f "$SCRIPT_DIR/Dockerfile" -t "mm0n0/test_host_$PROJECT_NAME:v1" "$SCRIPT_DIR"
